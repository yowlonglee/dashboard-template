jQuery(document).ready(function($) {
	var makeMenuOption = function(item){
		var menuOption = $('<a/>', {
			text: item.text,
			href: item.url
		});
		return $('<li/>').append(menuOption);
	};

	// loop through the data and make submenus
	var makeDropdownSubmenu = function(data, indexArray){
		// creat a menu holder to store its menu options
		indexArray.menuHolder = $('<ul/>', {class: 'dropdown-menu'});
		$.each(data.items, function(x, item){
			// creat a new object in array to hold the variables in this iteration
			indexArray[x] = new Object();
			// check if any of the items in this data
			// has "items" property
			indexArray[x].submenuData = checkSubmenuData(item);
			indexArray[x].dataArray = new Array();
			if (indexArray[x].submenuData) {
				// if any one of the items in this iteration has child items
				// we call this function again untill the last menu
				indexArray[x].submenu = makeDropdownSubmenu(item, indexArray[x].dataArray);
				indexArray[x].submenu.appendTo(indexArray.menuHolder);
			}else{
				indexArray[x].option = makeMenuOption(item);
				indexArray[x].option.appendTo(indexArray.menuHolder);
			}
		});

		indexArray.menuToggle = makeMenuOption(data); // <li/><a/>
		indexArray.menuToggle.children('a').attr({
			'data-toggle': 'dropdown',
			'href': '#'
		});
		indexArray.menuToggle.attr({'class': 'dropdown-submenu'}).append(indexArray.menuHolder);
		return indexArray.menuToggle;
	};

	var makeDropdownMenu = function(dropdown){
		var dropdownMenuHolder = $('<ul/>', {class: 'dropdown-menu'});
		var array = [];
		// make the dropdown list
		$.each(dropdown, function(i, dropdownItem){

			var checkSubmenu = checkSubmenuData(dropdownItem);
			if (checkSubmenu) {
				array[i] = new Object();
				var dropdownSubmenu = makeDropdownSubmenu(dropdownItem, array[i]);
				dropdownSubmenu.appendTo(dropdownMenuHolder);
			}else{
				var option = makeMenuOption(dropdownItem);
				option.appendTo(dropdownMenuHolder);
			}

		});

		return dropdownMenuHolder;
	};

	var makeDropdownToggle = function(el){
		el.attr({
			'class': 'dropdown-toggle',
			'data-toggle': 'dropdown',
			'role': 'button',
			'aria-haspopup': true,
			'aria-expanded': false
		}).append($('<span/>', {class: 'caret'}));
		return el;
	};

  // check if current object has items property
  // if true, return the items array
  var checkSubmenuData = function(src){
  		if (src.items) {
  			return src.items;
  		}else{
  			return false;
  		}
  };

	var makeNavbar = function(src){
		// nav buttons which appear on the navbar
		var navs = src.models;

		var navLists = $('<ul/>', {class: 'nav navbar-nav'});

		$.each( navs, function(index, navItem){
			var navTitle = navItem.text;
			var navUrl = navItem.url;

			var dropdown = checkSubmenuData(navItem);

			var navList = $('<li/>');
			var navAnchor = $('<a/>', {
				text: navTitle,
				href: '#'
			});

			if (dropdown) {
				// if this listitem has a child dropdown list
				// make its dropdown menu
				var dropdownMenu = makeDropdownMenu(dropdown);

				// turn its anchor to toggle button
				makeDropdownToggle(navAnchor);
				// add className to its parent li element and append the toggle button and dropdown menu
				navList.attr({
					'class': 'dropdown'
				}).append(navAnchor).append(dropdownMenu);
			} else {
				navAnchor.attr('href', navUrl);
				navList.append(navAnchor)
			}
			navList.appendTo(navLists);
		});
		return navLists;
	};

	var nav = makeNavbar(navSource);
	$('#navbar').append(nav);

	$('#navbar').on('click', 'li.dropdown-submenu', function(event){
    event.stopPropagation();
    event.preventDefault();
		var currentTarget = $(event.currentTarget);
		if (currentTarget.hasClass('open')) {
			// if the submenu is open
			// remove all open classname in child elements
			currentTarget.find('li.dropdown-submenu').removeClass('open');
			currentTarget.removeClass('open');
		}else{
      currentTarget.addClass('open');
		}
	});

});