var navSource = {
  "total":null,
  "status":true,
  "message":"",
  "models":[
    {
      "imageUrl":"../../resources/images/star.png",
      "text":"查看狀態",
      "items":[
        {
          "imageUrl":null,
          "text":"訂單匯入狀態",
          "url":"add-orders.html"
        },
        {
          "imageUrl":null,
          "text":"會員資料狀態",
          "url":"add-members.html"
        },
        {
          "imageUrl":null,
          "text":"dropdown",
          "url":null,
          "items":[
            {
              "imageUrl":null,
              "text":"submenu 1-3-1",
              "url":"../rsUser/init"
            },
            {
              "imageUrl":null,
              "text":"submenu 1-3-2",
              "url":null,
              "items":[
                {
                  "imageUrl":null,
                  "text":"submenu 1-3-2-1",
                  "url":null,
                  "items":[
                    {
                      "imageUrl":null,
                      "text":"submenu 1-3-2-1-1",
                      "url": "../rsUser/init"
                    },
                    {
                      "imageUrl":null,
                      "text":"submenu 1-3-2-1-2",
                      "url": "../rsUser/init"
                    }
                  ]
                },
                {
                  "imageUrl":null,
                  "text":"submenu 1-3-2-2",
                  "url":"../rsUser/init"
                },
              ]
            },
            {
              "imageUrl":null,
              "text":"submenu 1-3-3",
              "url":"../rsUser/init"
            }
          ]
        },
        {
          "imageUrl":null,
          "text":"分析狀態",
          "url":"add-analysis.html"
        }
      ],
      "url":null
    },
    {
      "imageUrl":"../../resources/images/star.png",
      "text":"查看圖表",
      "url":"charts.html"
    },
    {
      "imageUrl":"../../resources/images/star.png",
      "text":"RFM參數設定",
      "url":"config-bin.html"
    },
    {
      "imageUrl":"../../resources/images/star.png",
      "text":"推播清單",
      "url":"../broadcast/init"
    }
  ]
};