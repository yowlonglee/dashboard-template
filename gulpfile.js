var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');
var msgSass = 'Gulp is compiling Sass. _φ(･_･';
var msgScript = 'Minifing JS';
var msgError = 'Ouch! Gulp caught error! (╯°□°）╯︵ ┻━┻';

gulp.task('sass', function() {
  return gulp.src('src/scss/*.scss')
    .pipe(plumber({
      errorHandler: notify.onError(function(error){
        return error.message
      })
    }))
    .pipe(sass({
      // outputStyle: 'compressed',
    }))
    .pipe(notify(msgSass))
    .pipe(gulp.dest('dist/css'))
  	.pipe(browserSync.stream());
});

gulp.task('script', function(){
  return gulp.src('src/js/*.js')
      // .pipe(uglify())
    .pipe(notify({
      message: msgScript,
      onLast: true
    }))
    .pipe(gulp.dest('dist/js'));

});

gulp.task('js-watch', ['script'], function(done){
  browserSync.reload();
  done();
});

gulp.task('watch', function(){
	browserSync.init({
		server: true,
		browser: 'Google Chrome Canary'
	});

  gulp.watch(['src/scss/*.scss', 'src/scss/modules/*.scss'], ['sass']);
  gulp.watch(['src/js/*.js'], ['js-watch']);
  gulp.watch(['*.html']).on('change', browserSync.reload);
});
gulp.task('default', ['watch']);